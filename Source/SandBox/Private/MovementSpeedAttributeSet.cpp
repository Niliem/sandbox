// Fill out your copyright notice in the Description page of Project Settings.


#include "MovementSpeedAttributeSet.h"

UMovementSpeedAttributeSet::UMovementSpeedAttributeSet()
{
  InitWalkingSpeed(165.0f);
  InitRunningSpeed(350.0f);
  InitSprintingSpeed(600.0f);
  InitCrouchingSpeed(150.0f);
}