
#include "UpdateRateOptimizationComponent.h"
#include "Components/SkinnedMeshComponent.h"

UUpdateRateOptimizationComponent::UUpdateRateOptimizationComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UUpdateRateOptimizationComponent::BeginPlay()
{
	Super::BeginPlay();

	if( m_LODFrameRate.Num() <= 0 )
		return;

	for( auto& component : GetOwner()->K2_GetComponentsByClass( USkinnedMeshComponent::StaticClass() ) )
	{
		if( USkinnedMeshComponent* skinnedMesh = Cast<USkinnedMeshComponent>( component ) )
		{
			skinnedMesh->bEnableUpdateRateOptimizations = true;
			skinnedMesh->AnimUpdateRateParams->bShouldUseLodMap = true;

			for( int i = 0; i < m_LODFrameRate.Num(); ++i )
			{
				skinnedMesh->AnimUpdateRateParams->LODToFrameSkipMap.Add( i, m_LODFrameRate[i] );
			}
		}
	}
}

