// Fill out your copyright notice in the Description page of Project Settings.


#include "LocoCharacterGAS.h"
#include "AbilitySystemComponent.h"

#include "MovementSpeedAttributeSet.h"
#include "MovementSpeedModAttributeSet.h"
#include "HealthAttributeSet.h"

ALocoCharacterGAS::ALocoCharacterGAS( const class FObjectInitializer& ObjectInitializer )
  : Super( ObjectInitializer.SetDefaultSubobjectClass<ULocoCharacterMovComponentGAS>( ACharacter::CharacterMovementComponentName ) )
{
  AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>( "AbilitySystemComponent" );

  MovementSpeedAttributeSet = CreateDefaultSubobject<UMovementSpeedAttributeSet>( "MovementSpeedAttributeSet" );
  MovementSpeedModifiersAttributeSet = CreateDefaultSubobject<UMovementSpeedModAttributeSet>( "MovementSpeedModifiersAttributeSet" );
  HealthAttributeSet = CreateDefaultSubobject<UHealthAttributeSet>( "HealthAttributeSet" );
}

UAbilitySystemComponent* ALocoCharacterGAS::GetAbilitySystemComponent() const
{
  return AbilitySystemComponent;
}

void ALocoCharacterGAS::BeginPlay()
{
  Super::BeginPlay();

  if( !IsValid(AbilitySystemComponent) )
    return;
  
  AbilitySystemComponent->InitAbilityActorInfo(this, this);
}

void ALocoCharacterGAS::AquireAbility( TSubclassOf<UGameplayAbility> AbilityToAquire )
{
  if( !IsValid(AbilitySystemComponent) )
    return;
  
  if( HasAuthority() && AbilityToAquire )
  {
    AbilitySystemComponent->GiveAbility( FGameplayAbilitySpec( AbilityToAquire, 1, 0 ) );
  }
  AbilitySystemComponent->InitAbilityActorInfo( this, this );
}

//

float ALocoCharacterGAS::GetWalkingSpeed() const
{
  if( MovementSpeedAttributeSet )
  {
    return MovementSpeedAttributeSet->GetWalkingSpeed();
  }
  return 0.0f;
}

float ALocoCharacterGAS::GetRunningSpeed() const
{
  if( MovementSpeedAttributeSet )
  {
    return MovementSpeedAttributeSet->GetRunningSpeed();
  }
  return 0.0f;
}

float ALocoCharacterGAS::GetSprintingSpeed() const
{
  if( MovementSpeedAttributeSet )
  {
    return MovementSpeedAttributeSet->GetSprintingSpeed();
  }
  return 0.0f;
}

float ALocoCharacterGAS::GetCrouchingSpeed() const
{
  if( MovementSpeedAttributeSet )
  {
    return MovementSpeedAttributeSet->GetCrouchingSpeed();
  }
  return 0.0f;
}

//

float ALocoCharacterGAS::GetWalkingSpeedModifier() const
{
  if( MovementSpeedModifiersAttributeSet )
  {
    return MovementSpeedModifiersAttributeSet->GetWalkingSpeedModifier();
  }
  return 1.0f;
}

float ALocoCharacterGAS::GetRunningSpeedModifier() const
{
  if( MovementSpeedModifiersAttributeSet )
  {
    return MovementSpeedModifiersAttributeSet->GetRunningSpeedModifier();
  }
  return 1.0f;
}

float ALocoCharacterGAS::GetSprintingSpeedModifier() const
{
  if( MovementSpeedModifiersAttributeSet )
  {
    return MovementSpeedModifiersAttributeSet->GetSprintingSpeedModifier();
  }
  return 1.0f;
}

float ALocoCharacterGAS::GetCrouchingSpeedModifier() const
{
  if( MovementSpeedModifiersAttributeSet )
  {
    return MovementSpeedModifiersAttributeSet->GetCrouchingSpeedModifier();
  }
  return 1.0f;
}

float ALocoCharacterGAS::GetMovementSpeedModifier() const
{
  if( MovementSpeedModifiersAttributeSet )
  {
    return MovementSpeedModifiersAttributeSet->GetMovementSpeedModifier();
  }
  return 1.0f;
}

//

float ALocoCharacterGAS::GetActualSpeed() const
{
  if( GetStance() == EStance::eCrouching )
  {
    switch( GetGait() )
    {
    case EGait::eWalking:
    case EGait::eRunning:
      return GetCrouchingSpeed() * GetCrouchingSpeedModifier() * GetMovementSpeedModifier();
    case EGait::eSprinting:
      return GetCrouchingSpeed() * GetCrouchingSpeedModifier() * GetMovementSpeedModifier() * 1.5f;
    }
  }
  else
  {
    switch( GetGait() )
    {
    case EGait::eWalking:
      return GetWalkingSpeed() * GetWalkingSpeedModifier() * GetMovementSpeedModifier();
    case EGait::eRunning:
      return GetRunningSpeed() * GetRunningSpeedModifier() * GetMovementSpeedModifier();
    case EGait::eSprinting:
      return GetSprintingSpeed() * GetSprintingSpeedModifier() * GetMovementSpeedModifier();
    }
  }

  return 0.0f;
}

float ALocoCharacterGAS::GetCurrentBaseSpeed() const
{
  if( GetStance() == EStance::eCrouching )
  {
    return GetCrouchingSpeed();
  }
  else
  {
    return GetRunningSpeed();
  }
  return 0.0f;
}
