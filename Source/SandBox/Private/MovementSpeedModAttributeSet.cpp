// Fill out your copyright notice in the Description page of Project Settings.


#include "MovementSpeedModAttributeSet.h"

UMovementSpeedModAttributeSet::UMovementSpeedModAttributeSet()
{
  InitWalkingSpeedModifier( 1.0f );
  InitRunningSpeedModifier( 1.0f );
  InitSprintingSpeedModifier( 1.0f );
  InitCrouchingSpeedModifier( 1.0f );
  InitMovementSpeedModifier( 1.0f );
}