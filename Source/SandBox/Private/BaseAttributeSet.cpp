// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"


void UBaseAttributeSet::AdjustAttributeForMaxChange( FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty )
{
  UAbilitySystemComponent* AbilitySystemComponent = GetOwningAbilitySystemComponent();
  const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
  if( !FMath::IsNearlyEqual( CurrentMaxValue, NewMaxValue ) && IsValid( AbilitySystemComponent ) )
  {
    const float CurrentValue = AffectedAttribute.GetCurrentValue();
    float NewDelta = (CurrentMaxValue > 0.0f) ? ((CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue)
      : NewMaxValue;

    AbilitySystemComponent->ApplyModToAttributeUnsafe( AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta );
  }
}