// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"

UHealthAttributeSet::UHealthAttributeSet()
{
    InitHealth(100.0f);
    InitMaxHealth(100.0f);
    InitHealthRegen(0.0f);
}

void UHealthAttributeSet::PreAttributeChange( const FGameplayAttribute& Attribute, float& NewValue )
{
  Super::PreAttributeChange( Attribute, NewValue );

  if( Attribute == GetMaxHealthAttribute() )
  {
    AdjustAttributeForMaxChange( Health, MaxHealth, NewValue, GetHealthAttribute() );
  }
}

void UHealthAttributeSet::PostGameplayEffectExecute( const FGameplayEffectModCallbackData& Data )
{
  Super::PostGameplayEffectExecute( Data );

  if( Data.EvaluatedData.Attribute == GetHealthAttribute() )
  {
    SetHealth( FMath::Clamp( GetHealth(), 0.0f, GetMaxHealth() ) );
  }
}

