// Fill out your copyright notice in the Description page of Project Settings.


#include "LocoCharacterMovementComponent.h"

bool ULocoCharacterMovementComponent::HasMovementInput() const
{
  return (GetCurrentAcceleration().Size() / GetMaxAcceleration()) > 0.0f;
}

EMovementState ULocoCharacterMovementComponent::GetMovementState() const
{
  switch( MovementMode )
  {
  case EMovementMode::MOVE_None:
    break;
  case EMovementMode::MOVE_Walking:
  case EMovementMode::MOVE_NavWalking:
    return EMovementState::eGrounded;
  case EMovementMode::MOVE_Falling:
    return EMovementState::eInAir;
  case EMovementMode::MOVE_Swimming:
  case EMovementMode::MOVE_Flying:
    break;
  case EMovementMode::MOVE_Custom:
    return static_cast<EMovementState>(CustomMovementMode);
  }

  return EMovementState::eNone;
}

void ULocoCharacterMovementComponent::SetGait( EGait NewGait )
{
  if( NewGait != Gait )
  {
    Gait = NewGait;
  }
}

EGait ULocoCharacterMovementComponent::GetGait() const
{
  return Gait;
}

EStance ULocoCharacterMovementComponent::GetStance() const
{
  if( IsCrouching() )
    return EStance::eCrouching;
  return EStance::eStanding;
}
