#include "LocoCharacter.h"

ALocoCharacter::ALocoCharacter( const class FObjectInitializer& ObjectInitializer )
	: Super( ObjectInitializer.SetDefaultSubobjectClass<ULocoCharacterMovementComponent>( ACharacter::CharacterMovementComponentName ) )
{
	PrimaryActorTick.bCanEverTick = false;
}

EGait ALocoCharacter::GetGait() const
{
	if( auto locoMovement = GetLocoMovement() )
		return locoMovement->GetGait();
	return EGait::eWalking;
}

EGait ALocoCharacter::GetDesiredGait() const
{
	return DesiredGait;
}

void ALocoCharacter::SetDesiredGait( EGait NewDesiredGait )
{
	DesiredGait = NewDesiredGait;
}

void ALocoCharacter::UpdateGait()
{
	if( auto locoMovement = GetLocoMovement() )
	{
		EGait allowedGait = GetAllowedGait();
		EGait currentGait = locoMovement->GetGait();

		if( currentGait != allowedGait )
		{
			locoMovement->SetGait( allowedGait );

			OnGaitChanged( currentGait, allowedGait );
		}
	}
}

EStance ALocoCharacter::GetStance() const
{
	if( auto locoMovement = GetLocoMovement() )
		return locoMovement->GetStance();
	return EStance::eStanding;
}

bool ALocoCharacter::HasMovementInput() const
{
	if( auto locoMovement = GetLocoMovement() )
		return locoMovement->HasMovementInput();
	return false;
}

EMovementState ALocoCharacter::GetMovementState() const
{
	if( auto locoMovement = GetLocoMovement() )
		return locoMovement->GetMovementState();
	return EMovementState::eNone;
}

