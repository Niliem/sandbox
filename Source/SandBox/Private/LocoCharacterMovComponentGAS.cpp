// Fill out your copyright notice in the Description page of Project Settings.


#include "LocoCharacterMovComponentGAS.h"
#include "LocoCharacterGAS.h"

float ULocoCharacterMovComponentGAS::GetMaxSpeed() const
{
  ALocoCharacterGAS* Owner = Cast<ALocoCharacterGAS>( GetOwner() );
	if( !Owner )
	{
		UE_LOG( LogTemp, Error, TEXT( "%s() No Owner" ), TEXT( __FUNCTION__ ) );
		return Super::GetMaxSpeed();
	}

	return Owner->GetActualSpeed();
}