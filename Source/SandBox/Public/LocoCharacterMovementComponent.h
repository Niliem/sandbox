// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Enums/EnumStance.h"
#include "Enums/EnumMovementState.h"
#include "Enums/EnumGait.h"
#include "LocoCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API ULocoCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

  UFUNCTION( BlueprintCallable, Category = "MovementInput" )
    bool HasMovementInput() const;

  UFUNCTION( BlueprintCallable, Category = "Locomotion" )
    EMovementState GetMovementState() const;

  // Gait 

  UFUNCTION( BlueprintCallable, Category = "Locomotion" )
    EGait GetGait() const;

  UFUNCTION( BlueprintCallable, Category = "Locomotion" )
    void SetGait( EGait NewGait);

  // Stance

  UFUNCTION( BlueprintCallable, Category = "Locomotion" )
    EStance GetStance() const;

protected:

  UPROPERTY( BlueprintReadOnly, Category = "Locomotion" )
    EGait Gait;
};
