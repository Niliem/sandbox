// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AttributeAccessors.h"
#include "BaseAttributeSet.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API UBaseAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

protected:

  void AdjustAttributeForMaxChange( FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty );
	
};
