// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LocoCharacterMovementComponent.h"
#include "LocoCharacterMovComponentGAS.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API ULocoCharacterMovComponentGAS : public ULocoCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
  virtual float GetMaxSpeed() const override;
};
