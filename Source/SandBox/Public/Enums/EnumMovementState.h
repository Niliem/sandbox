// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnumMovementState.generated.h"

UENUM( BlueprintType )
enum class EMovementState : uint8
{
  eNone     UMETA( DisplayName = "None" ),
  eGrounded UMETA( DisplayName = "Grounded" ),
  eInAir    UMETA( DisplayName = "InAir" ),
  eRagdoll  UMETA( DisplayName = "Ragdoll" )
};