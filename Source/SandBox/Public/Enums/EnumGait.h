// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnumGait.generated.h"


UENUM( BlueprintType )
enum class EGait : uint8
{
  eWalking  UMETA( DisplayName = "Walking" ),
  eRunning  UMETA( DisplayName = "Running" ),
  eSprinting UMETA( DisplayName = "Sprinting" )
};