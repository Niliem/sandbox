#pragma once

#include "CoreMinimal.h"
#include "EnumStance.generated.h"

UENUM( BlueprintType )
enum class EStance : uint8
{
  eStanding  UMETA( DisplayName = "Standing" ),
  eCrouching UMETA( DisplayName = "Crouching" )
};