// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseAttributeSet.h"
#include "MovementSpeedAttributeSet.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API UMovementSpeedAttributeSet : public UBaseAttributeSet
{
	GENERATED_BODY()

public:
  UMovementSpeedAttributeSet();

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeed" )
    FGameplayAttributeData WalkingSpeed;
  ATTRIBUTE_ACCESSORS( UMovementSpeedAttributeSet, WalkingSpeed )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeed" )
    FGameplayAttributeData RunningSpeed;
  ATTRIBUTE_ACCESSORS( UMovementSpeedAttributeSet, RunningSpeed )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeed" )
    FGameplayAttributeData SprintingSpeed;
  ATTRIBUTE_ACCESSORS( UMovementSpeedAttributeSet, SprintingSpeed )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeed" )
    FGameplayAttributeData CrouchingSpeed;
  ATTRIBUTE_ACCESSORS( UMovementSpeedAttributeSet, CrouchingSpeed )
	
};
