#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "LocoCharacterMovementComponent.h"
#include "LocoCharacter.generated.h"


UCLASS()
class SANDBOX_API ALocoCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ALocoCharacter( const class FObjectInitializer& ObjectInitializer );

	// GAIT

	UFUNCTION( BlueprintCallable, BlueprintImplementableEvent, Category = "Locomotion" )
		EGait GetAllowedGait() const;

	UFUNCTION( BlueprintCallable, Category = "Locomotion" )
		EGait GetGait() const;

	UFUNCTION( BlueprintCallable, Category = "Locomotion" )
		EGait GetDesiredGait() const;

	UFUNCTION( BlueprintCallable, Category = "Locomotion" )
		void SetDesiredGait( EGait NewDesiredGait );

	UFUNCTION( BlueprintCallable, Category = "Locomotion" )
		void UpdateGait();

	UFUNCTION( BlueprintImplementableEvent, Category = "Locomotion" )
		void OnGaitChanged( EGait OldGait, EGait NewGait );

	UFUNCTION( BlueprintCallable, Category = "Locomotion" )
		EStance GetStance() const;

	UFUNCTION( BlueprintCallable, Category = "MovementInput" )
		bool HasMovementInput() const;

	UFUNCTION( BlueprintCallable, Category = "Locomotion" )
		EMovementState GetMovementState() const;

	// INPUTS

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Locomotion|Input" )
		EGait DesiredGait;	

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Locomotion|Input" )
		EStance DesiredStance;

public:
	FORCEINLINE ULocoCharacterMovementComponent* GetLocoMovement() const { return Cast<ULocoCharacterMovementComponent>(GetCharacterMovement()); };

};
