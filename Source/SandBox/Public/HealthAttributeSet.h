// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseAttributeSet.h"
#include "HealthAttributeSet.generated.h"

/**
 * 
 */

UCLASS()
class SANDBOX_API UHealthAttributeSet : public UBaseAttributeSet
{
	GENERATED_BODY()

public:
  UHealthAttributeSet();

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|Health" )
    FGameplayAttributeData Health;
  ATTRIBUTE_ACCESSORS( UHealthAttributeSet, Health )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|Health" )
    FGameplayAttributeData MaxHealth;
  ATTRIBUTE_ACCESSORS( UHealthAttributeSet, MaxHealth )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|Health" )
    FGameplayAttributeData HealthRegen;
  ATTRIBUTE_ACCESSORS( UHealthAttributeSet, HealthRegen )

  virtual void PreAttributeChange( const FGameplayAttribute& Attribute, float& NewValue ) override;
  virtual void PostGameplayEffectExecute( const struct FGameplayEffectModCallbackData& Data ) override;

};
