// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseAttributeSet.h"
#include "MovementSpeedModAttributeSet.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API UMovementSpeedModAttributeSet : public UBaseAttributeSet
{
	GENERATED_BODY()

public:
  UMovementSpeedModAttributeSet();

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeedModifiers" )
    FGameplayAttributeData WalkingSpeedModifier;
  ATTRIBUTE_ACCESSORS( UMovementSpeedModAttributeSet, WalkingSpeedModifier )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeedModifiers" )
    FGameplayAttributeData RunningSpeedModifier;
  ATTRIBUTE_ACCESSORS( UMovementSpeedModAttributeSet, RunningSpeedModifier )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeedModifiers" )
    FGameplayAttributeData SprintingSpeedModifier;
  ATTRIBUTE_ACCESSORS( UMovementSpeedModAttributeSet, SprintingSpeedModifier )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeedModifiers" )
    FGameplayAttributeData CrouchingSpeedModifier;
  ATTRIBUTE_ACCESSORS( UMovementSpeedModAttributeSet, CrouchingSpeedModifier )

  UPROPERTY( BlueprintReadOnly, Category = "Attributes|MovementSpeedModifiers" )
    FGameplayAttributeData MovementSpeedModifier;
  ATTRIBUTE_ACCESSORS( UMovementSpeedModAttributeSet, MovementSpeedModifier )
	
};
