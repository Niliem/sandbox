// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LocoCharacter.h"
#include "AbilitySystemInterface.h"
#include "LocoCharacterMovComponentGAS.h"

#include "LocoCharacterGAS.generated.h"

class UGameplayAbility;
class UGameplayEffect;
class UAbilitySystemComponent;
class UAttributeSet;
class UMovementSpeedAttributeSet;
class UMovementSpeedModAttributeSet;
class UHealthAttributeSet;

/**
 * 
 */
UCLASS()
class SANDBOX_API ALocoCharacterGAS : public ALocoCharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
  ALocoCharacterGAS( const class FObjectInitializer& ObjectInitializer );  

  // Implement IAbilitySystemInterface
  virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

  virtual void BeginPlay() override;

  UFUNCTION( BlueprintCallable, Category = "Ability" )
    void AquireAbility( TSubclassOf<UGameplayAbility> AbilityToAquire );

  // GETTERS

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetWalkingSpeed() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetRunningSpeed() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetSprintingSpeed() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetCrouchingSpeed() const;

  // 

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetWalkingSpeedModifier() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetRunningSpeedModifier() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetSprintingSpeedModifier() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetCrouchingSpeedModifier() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetMovementSpeedModifier() const;

  //

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetActualSpeed() const;

  UFUNCTION( BlueprintCallable, Category = "Attributes" )
    float GetCurrentBaseSpeed() const;

protected:
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UAbilitySystemComponent* AbilitySystemComponent;

  UPROPERTY()
    UMovementSpeedAttributeSet* MovementSpeedAttributeSet;

  UPROPERTY()
    UMovementSpeedModAttributeSet* MovementSpeedModifiersAttributeSet;
    
  UPROPERTY()
    UHealthAttributeSet* HealthAttributeSet;
	
public:
	FORCEINLINE ULocoCharacterMovComponentGAS* GetLocoMovement() const { return Cast<ULocoCharacterMovComponentGAS>(GetCharacterMovement()); };

};
