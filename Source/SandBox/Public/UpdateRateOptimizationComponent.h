// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UpdateRateOptimizationComponent.generated.h"


UCLASS( ClassGroup=(Custom), Blueprintable, meta=(BlueprintSpawnableComponent) )
class SANDBOX_API UUpdateRateOptimizationComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UUpdateRateOptimizationComponent();

	virtual void BeginPlay() override;

private:
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "UpdateRateUptimization", meta = (AllowPrivateAccess = "true") )
		TArray<int> m_LODFrameRate;
};
